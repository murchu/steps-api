var express = require('express');
var logger = require('morgan');
var cors = require('cors')

var app = express();
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static('api-docs'));

var swaggerJSDoc = require('swagger-jsdoc');
const packageJson = require('../package.json');

var stepsRouter = require('./routes/stepsRoutes');

// swagger definition
var swaggerDefinition = {
  info: {
    title: 'Step Calculations API',
    version: packageJson.version,
    description: 'Helping you never waste a stride!',
  },
  host: 'localhost:3000',
  basePath: '/api/v1/',
};

// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./**/routes/*.js','routes.js'],// pass all in array 

  };

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);

// routes and routers
app.get('/api/v1/swagger.json', function(req, res) {   res.setHeader('Content-Type', 'application/json');   res.send(swaggerSpec); });
app.use('/api/v1/steps', stepsRouter);

module.exports = app;
