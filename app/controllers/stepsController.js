'use strict';

const stepsUtils = require('../utils/stepsUtils');

/**
 * Calculates the minimum number of strides to climb flights of stairs 
 * @param {number[]} flights An array of flights. Each entry is a flight and the number of steps in it
 * @param {number} stride The number of steps taken with each stride
 * @returns {Object} The minimum number of strides to climb the stairs
 */
exports.minimumStrides = function(requestData) {
  const flights = requestData.flights;
  const stride = requestData.stride;

  const validationResponse = stepsUtils.validateInput(flights, stride);

  let response = {};

  if (validationResponse.error) {
    response.error = validationResponse.error;
  } else {
    let minStrides = 0;

    // min strides for flights
    for (let i = 0; i < flights.length; i++) {
      minStrides += Math.ceil(flights[i] / stride);
    }   

    // 2 strides for each landing between flights
    const numberLandings = flights.length -1;
    minStrides += (numberLandings * 2)

    response.result = minStrides;
  }

  return response;
  
};
