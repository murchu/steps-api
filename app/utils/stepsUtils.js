'use strict';

/**
 * Checks whether flights data is valid
 * @param {number[]} flights Each value is a flight and the number of steps in it
 * @returns {Object} Returned object contains an 'error' value if flights is invalid
 */
function validateFlights(flights) {
  let data = {};

  // must be flight values
  if (!flights || !(flights.length > 0)) {
    data.error = "No flight data submitted";
    return data;
  }

  // must be no more than 30 flights of stairs
  if (flights.length > 30) {
    data.error = "Number of flights submitted exceeds the maximum";
    return data;
  }

  // all flight values must be numbers
  if (flights.some(isNaN)) {
    data.error = "Invalid values submitted for flight or stride value(s)";
    return data;
  }

  // must be no more than 20 steps in each flight
  const invalidVals = flights.some((flight) => {
    return flight < 0 || flight > 20;
  });
  if (invalidVals) {
    data.error = "1 or more of the submitted flight size(s) are invalid"
    return data;
  }
  return data;
}

/**
 * Checks whether stride data is valid
 * @param {number} stride The number of steps in the stride
 * @returns {Object} Returned object contains an 'error' value if stride is invalid
 */
function validateStride(stride) {
  let data = {};

  // must be a stride value
  if (!stride) {
    data.error = "No stride data submitted";
    return data;
  }

  // stride value must be a number between 0 - 4
  if (stride < 0 || stride > 4 || isNaN(stride)) {
    data.error = "Stride value submitted is invalid"
    return data;
  }

  return data;
}

/**
* Checks whether a data object contains valid flights and stride data to 
* calculate the minimum number of strides to climb the submitted flights
* @param {Object} requestData An object 
* @returns {Object} An object with an 'error' key if invalid, otherwise returns an empty object
*/
exports.validateInput = function(flights, stride) {
  let validationResponse = {};

  // validate first as will return faster
  const strideValidationResponse = validateStride(stride);
  if (strideValidationResponse.error) {
    return strideValidationResponse;
  }

  const flightValidationResponse = validateFlights(flights);
  if (flightValidationResponse.error) {
    return flightValidationResponse;
  }

  return validationResponse;
}