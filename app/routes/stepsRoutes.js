'use strict';

const router = require('express').Router();
const multer = require('multer');
const upload = multer();
const stepsController = require('../controllers/stepsController');

/**
 * @swagger
 * definition:
 *   flight-data:
 *     properties:
 *       flights:
 *         description: number of flights of stairs
 *         type: array
 *         minItems: 1
 *         maxItems: 30
 *         items: 
 *           type: integer
 *           minimum: 1
 *           maximum: 20
 *       stride:
 *         description: size of stride in steps
 *         type: integer
 *         minimum: 1
 *         maximum: 4
 * 
 */

/**
 * @swagger
 * /steps:
 *   post:
 *     tags:
 *       - steps
 *     description: Calculates the minimum strides required to climb stairs
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: flight-data
 *         description: user object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/flight-data'
 *     responses:
 *       200:
 *         description: Successfully calculated minimum strides required.
 *       400:
 *         description: Unable to calculate - input received was invalid.
 */
router.post('/', upload.array(), function(req, res) {
  const requestData = req.body;
  const jsonReponse = stepsController.minimumStrides(requestData);
  if (jsonReponse.error) {
    res.status(400).json(jsonReponse);
  } else if (jsonReponse.result) {
    res.status(200).json(jsonReponse);
  } else {
    res.status(500).json({ error: 'server unable to process request.' });
  }
});

module.exports = router;
