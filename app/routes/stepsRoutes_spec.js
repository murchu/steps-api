"use strict";

process.env.NODE_ENV = "test";
const server = require("../../index");

let chai = require('chai');
let should = chai.should();
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

const fixtures = {
  "flightsNone": [],
  "flightsNormal": [ 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 ],
  "flightsTooLarge": [ 21, 20, 19 ],
  "flightsTooMany": [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ],
  "flightsInvalid": [ -12, "&", "L" ],
  "strideNone": "",
  "strideNormal": "2",
  "strideTooLarge": "5",
  "strideInvalid": "-1"
}

describe("/steps endpoints", function() {

  describe("/POST steps", function() {
    it("should return the right answer with good input", (done) => {
      let obj = {};
      obj.flights = fixtures.flightsNormal;
      obj.stride = fixtures.strideNormal;
  
      chai.request(server)
        .post('/api/v1/steps')
        .send(obj)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('result').eql(68);
          done();
        });
    });

    it("should return an error when no flight data attribute in input", (done) => {
      let obj = {};
      obj.stride = fixtures.strideNormal;
  
      chai.request(server)
        .post('/api/v1/steps')
        .send(obj)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });      

    });

    it("should return an error when no flight data values in input", (done) => {
      let obj = {};
      obj.flights = fixtures.flightsNone;
      obj.stride = fixtures.strideNormal;
      
      chai.request(server)
        .post('/api/v1/steps')
        .send(obj)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });  

    });

    it("should return an error when no stride data attribute in input", (done) => {
      let obj = {};
      obj.flights = fixtures.flightsNormal
  
      chai.request(server)
        .post('/api/v1/steps')
        .send(obj)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });      

    });

    it("should return an error when no stride data values in input", (done) => {
      let obj = {};
      obj.flights = fixtures.flightsNormal;
      obj.stride = fixtures.strideNone;
      
      chai.request(server)
        .post('/api/v1/steps')
        .send(obj)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });  

    });

    it("should return an error with invalid flight data input", (done) => {
      let obj = {};
      obj.flights = fixtures.flightsInvalid;
      obj.stride = fixtures.strideNormal;
  
      chai.request(server)
        .post('/api/v1/steps')
        .send(obj)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it("should return an error with invalid stride data input", (done) => {      
      let obj = {};
      obj.flights = fixtures.flightsNormal;
      obj.stride = fixtures.strideInvalid;
  
      chai.request(server)
        .post('/api/v1/steps')
        .send(obj)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it("should return an error when too many flights", (done) => {
      let obj = {};
      obj.flights = fixtures.flightsTooMany;
      obj.stride = fixtures.strideNormal;
  
      chai.request(server)
        .post('/api/v1/steps')
        .send(obj)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it("should return an error when flights too large", (done) => {
      let obj = {};
      obj.flights = fixtures.flightsTooLarge;
      obj.stride = fixtures.strideNormal;
  
      chai.request(server)
        .post('/api/v1/steps')
        .send(obj)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it("should return an error when stride too large", (done) => {
      let obj = {};
      obj.flights = fixtures.flightsNormal;
      obj.stride = fixtures.strideTooLarge;
  
      chai.request(server)
        .post('/api/v1/steps')
        .send(obj)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

  })  

});
