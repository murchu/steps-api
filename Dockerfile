FROM node:10-slim

# Create app directory
WORKDIR /usr/src/app

# Ensure both package.json AND package-lock.json (where available) are copied
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

EXPOSE 3000
CMD [ "npm", "start" ]