# steps-api

Simple API service written in NodeJS that calculates the minimum number of physical strides required to climb to the top of a stairwell.

## Installation

There are several installation options which are covered in more detail below
- install and run the server locally if you have a local NodeJS enviroment
- build a local docker image and run it yourself
- fetch a pre-built docker image and run it

If you would prefer not to host the service locally and just wish to evaluate the api, a hosted version is available [here](https://steps-api.glitch.me/).

### Build locally:

```shell
// clone repo
git clone <repo>

// navigate to cloned code
cd steps-api

// install dependencies
npm install

// start api server
npm start
```

### Build own docker image:

```bash
// clone repo
git clone <repo>

// navigate to cloned code
cd steps-api

// build docker app image
docker build -t steps-api:1.0.0 .

// run docker app container from built image
docker run --name steps-api -p 3000:3000 <image-ref>
```

### Use pre-built docker image:

```bash
// grab & run pre-built docker app image
docker run --name steps-api -p 3000:3000 murchu/steps-api:1.0.0
```

#### Note:

- The steps-api service serves on port 3000 by default
- Making a request to `localhost:3000` should verify if the service is up and running. 
- If you want the service to listen on a different port, you can do this by:
  - setting an env var `PORT` with your preferred port locally, or 
  - if running from a docker container via the `-p` and specifying a different port to listen on locally, ie `docker run --name steps-api -p 8000:3000 murchu/steps-api:1.0.0` 

## API Documentation

API documentation has been made available via SwaggerUI. To view the documentation simply navigate to `localhost:3000` once the service is running. 

## Developer notes

### Project structure & notes

The project is generally structured:

```
steps-api/
  api-docs/
  app/
    routes/
    app.js
  config/
  index.js
```
#### Some notes:
- App source code lives under `app/`
- App-specific config for your app is housed in `config/`
- `api-docs` simply houses the various files needed to render the SwaggerUI API documentation.
- `routes/` holds express routing objects & their associated route-handler code. 
- Test files live next to the file they test (`routes/steps.js` & `routes/steps_spec.js`) and adopt the `*_spec.js` naming convention. 
- `index.js` houses server-related boilerplate - server configuration is in `app/app.js`. 

### Tests

Tests are written with mocha and chai, and as mentioned live next to the files they test rather than in a seperate `test/` directory. They can be run with `npm test`, and new test files will be picked up once they adopt the `*_spec.js` naming convention.

### API documentation

API documentation can be added by adding JSDoc-style comments above the route. An example of this can be seen in `app/routes/steps.js`:

```javascript
/**
 * @swagger
 * /steps:
 *   post:
 *     tags:
 *       - steps
 *     description: Calculates the minimum strides required to climb stairs
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: flight-data
 *         description: user object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/flight-data'
 *     responses:
 *       200:
 *         description: Successfully calculated minimum strides required.
 *       400:
 *         description: Unable to calculate - input received was invalid.
 */
router.post('/', upload.array(), function(req, res) {
    const requestData = req.body;
    //validate input
    if (!validData(requestData)) {
        res.status(400).json({ error: 'input data was invalid - double check your query data.' });
    } else {
        const result = minimumStrides(requestData.flights, requestData.stride);
        let resp = {
            result: result
        };
        res.status(200).json(resp);
    }
});
```